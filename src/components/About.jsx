import React from 'react';

const BasicTable = () => {
  // Datos de ejemplo para la tabla
  const data = [
    { id: 1, name: 'Troopers', ubicacion: 'Lautaro' },
    { id: 2, name: 'Phantom', ubicacion: 'Lautaro' },
    { id: 3, name: 'Condor', ubicacion: 'Pillanlelbun' },
    { id: 4, name: 'O.T.G', ubicacion: 'Gorbea' },
    { id: 5, name: 'Husares', ubicacion: 'Valdivia' },
    { id: 6, name: 'Depredadores', ubicacion: 'Valdivia' },
  ];

  return (
    <div style={{ margin: '20px auto', maxWidth: '800px' }}> {/* Ancho máximo de la tabla */}
      <table style={{ width: '100%', borderCollapse: 'collapse' }}>
        <thead>
          <tr>
            <th style={{ padding: '12px', border: '1px solid #ddd', backgroundColor: '#f2f2f2', textAlign: 'left' }}>ID</th>
            <th style={{ padding: '12px', border: '1px solid #ddd', backgroundColor: '#f2f2f2', textAlign: 'left' }}>Nombre</th>
            <th style={{ padding: '12px', border: '1px solid #ddd', backgroundColor: '#f2f2f2', textAlign: 'left' }}>Ubicacion</th>
          </tr>
        </thead>
        <tbody>
          {data.map((item) => (
            <tr key={item.id}>
              <td style={{ padding: '12px', border: '1px solid #ddd' }}>{item.id}</td>
              <td style={{ padding: '12px', border: '1px solid #ddd' }}>{item.name}</td>
              <td style={{ padding: '12px', border: '1px solid #ddd' }}>{item.ubicacion}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default function About() {
  return (
    <>
      <main>
        <h2>Conoce los Equipos</h2>
        <p>Conoce los detalles de todos los equipos activos actualmente.</p>      
      </main>
      <div> <BasicTable /> </div> 
    </>
  );
}
