import { Layout, Home, About, Gallery, Contact, Inscripciones } from "./components";
import { Routes, Route } from "react-router-dom";

function App() {
  // Botón de descarga en Layout
  return (
    <Layout>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="galeria" element={<Gallery />} />
        <Route path="acerca" element={<About />} />
        <Route path="contacto" element={<Contact />} />
        <Route path="inscripciones" element={<Inscripciones />} />
      </Routes>
    </Layout>
  );
}

export default App;
